let url = "https://swapi.dev/api/people";

localStorage.user = JSON.stringify([]);

let array = [];

let data = fetch(url, {method: "get"});

// creating class
class Person {
    constructor(name, gender, birth_year, eye_color, hair_color) {
        this.name = name;
        this.gender = gender;
        this.birth_year = birth_year;
        this.eye_color = eye_color;
        this.hair_color = hair_color;
    }

    show() {
        // create card
        let card = document.createElement("div");
        card.className = "cards";
        document.querySelector(".person-card").append(card);

        let p1 = document.createElement("p");
        let p2 = document.createElement("p");
        let p3 = document.createElement("p");
        let p4 = document.createElement("p");
        let p5 = document.createElement("p");
        card.append(p1, p2, p3, p4, p5);

        //create button for save
        let btn = document.createElement("input");
        btn.setAttribute("type", "button");
        btn.setAttribute("value", "Зберегти");
        btn.setAttribute("id", this.name);

        p1.textContent = `Name: ${this.name}`;
        p2.textContent = `Gender: ${this.gender}`;
        p3.textContent = `Birth year: ${this.birth_year}`;
        p4.textContent = `Eye color: ${this.eye_color}`;
        p5.textContent = `Hair color: ${this.hair_color}`;
        p5.append(btn);
    }
}

let action = data.then((element) => element.json());


//create new persons
action.then((res) => {
    res.results.forEach(element => {
        let person = new Person(
            element.name,
            element.gender,
            element.birth_year,
            element.eye_color,
            element.hair_color
        );
        person.show();
        array.push(person);
    });
});

document.querySelector(".person-card").addEventListener("click", (e) => {
    if (e.target.type === "button") {
        array.forEach((el) => {
            if (el.name === e.target.id) {
                let a = JSON.parse(localStorage.user);
                a.push(el);
                localStorage.user = JSON.stringify(a);
            }
        })
    }
})
