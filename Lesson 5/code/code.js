let docum = {
    header: 0,
    body: 0,
    footer: 0,
    date: 0,
    application: {
        header: 0,
        body: 0,
        footer: 0,
        date: 0,
    },

    fillApp: function () {
        this.application.header = prompt("Введіть заголовок додатку");
        this.application.body = prompt("Додайте основний текст додатку");
        this.application.footer = prompt("Запишіть висновки та результати");
        this.application.date = prompt("Введіть сьогоднішню дату");
    },

    fillObject: function () {
        this.header = prompt("Введіть заголовок документу");
        this.body = prompt("Додайте основний текст документу");
        this.footer = prompt("Запишіть висновки та результати ");
        this.date = prompt("Введіть сьогоднішню дату для цього документу");

        if (confirm("Чи хочите додатково заповнити інформацію про додаток?")) {
            this.fillApp();
        }
    },

    displayObject: function () {
       document.write("Заголовок документу: " + this.header + "<br>");
       document.write("Основний текст: " + this.body + "<br>");
       document.write("Висновки: " + this.footer + "<br>");
       document.write("Дата: " + this.date + "<br>" + "<br>");

       document.write("Заголовок додатку: " + this.application.header + "<br>");
       document.write("Опис додатку: " + this.application.body + "<br>");
       document.write("Результат: " + this.application.footer + "<br>");
       document.write("Дата: " + this.application.date + "<br>");
    },
};

docum.fillObject();
docum.displayObject();
