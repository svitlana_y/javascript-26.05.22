const wallet = {
    name: "Ivan",
    btc: {
        name: "Bitcoine",
        logo: "<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/1.png'>",
        rate: "30245.99",
        coin: "54",
    },
    etn: {
        name:"Ethereum",
        logo:"<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/1027.png'>",
        rate:"1801.98",
        coin:"27",
      },
      xlm: {
        name:"Stellar",
        logo:"<img src='https://s2.coinmarketcap.com/static/img/coins/64x64/512.png'>",
        rate:"0.1402",
        coin:"37",
      },

      show: function(nameCoine) {
        document.getElementById("wallet").innerHTML = (`Добрый день, <span>${wallet.name}</span>! <br><br> На вашем балансе <span> ${wallet[nameCoine].name}</span>${wallet[nameCoine].logo} <br>осталось <span>${wallet[nameCoine].coin}</span> монет,<br> если вы сегодня продадите их то, получите <br> <span>${(wallet[nameCoine].rate*wallet[nameCoine].coin*31).toFixed(2)}</span>грн.`)
      }  
}

wallet.show(prompt("Веддіть назви монет:","btc, etn, xlm"));