const numbers = ["1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "."];
const operat = ["+", "-", "*", "/"];
const memory = ["mrc", "m-", "m+"];
let buttons;
let display;

let obj = {
    num1: "",
    num2: "",
    oper: "",
    rez: "",
    memr: "",
};

window.addEventListener("DOMContentLoaded", () => {
    buttons = document.querySelectorAll(".keys input");
    display = document.querySelector(".display > input");

    buttons.forEach((target) => {
        target.addEventListener("click", function (e) {

            let key = e.target.value;

            if (numbers.includes(key)) {
                inputNumber(key);
            }

            else if (operat.includes(key)) {
                inputOperator(key);
            }

            else if (memory.includes(key)) {
                inputMemory(key);
            }

            else if (key === "=") {
                inputEquals();
            }
            else if (key === "C") {
                clear();
                obj.rez = "";
                display.value = "";
            }

        })
    });
});

function inputNumber(key) {
    if (key === ".") {

        // which number to handle
        let handleNum1 = true;
        if (obj.oper) {
            handleNum1 = false
        }

        // get number value
        let numberValue = handleNum1 ? obj.num1 : obj.num2

        // check dot exist
        let isDotExist = (numberValue).includes(".");
        if (isDotExist) return;

        // check number is zero or empty
        let isZeroOrEmpty = numberValue === "0" || numberValue === "";
        if (isZeroOrEmpty) {
            if (handleNum1) {
                obj.num1 = "0.";
            }
            else {
                obj.num2 = "0.";
            }
        }
        else {
            if (handleNum1) {
                obj.num1 += key;
            }
            else {
                obj.num2 += key;
            }
        }

        // display value
        display.value = handleNum1 ? obj.num1 : obj.num2;
    }
    else if (obj.num1 === "" && obj.num2 === "") {

        obj.num1 = key;
        display.value = obj.num1;
    }
    else if (obj.num1 === "0") {
        key === "0" ? (obj.num1 = "0", display.value = obj.num1) : (obj.num1 = key,
            display.value = obj.num1);
    }
    else if (obj.num2 === "0") {
        key === "0" ? (obj.num2 = "0", display.value = obj.num2) : (obj.num2 = key,
            display.value = obj.num2);
    }
    else if (obj.num1 !== "" && obj.oper === "") {
        obj.num1 += key;
        display.value = obj.num1;
    }
    else if (obj.num1 !== "" && obj.oper !== "" && obj.num2 === "") {
        obj.num2 = key;
        display.value = obj.num2;
    }
    else if (obj.num1 !== "" && obj.oper !== "" && obj.num2 !== "") {
        obj.num2 += key;
        display.value = obj.num2;
    }
}

function inputOperator(key) {
    if (obj.num1 === "") {
        obj.num1 = obj.rez;
        obj.oper = key;
    }
    else if (obj.num1 !== "" && obj.num2 === "") {
        obj.oper = key;
    }
    else if (obj.num1 !== "" && obj.num2 !== "") {
        obj.rez = calculate(obj.num1, obj.num2, obj.oper);
        display.value = obj.rez;
        obj.num1 = obj.rez;
        obj.oper = key;
        obj.num2 = "";
    }
}

function inputEquals() {
    if (obj.num1 !== "" && obj.num2 !== "") {
        obj.rez = calculate(obj.num1, obj.num2, obj.oper);
        clear();
        display.value = obj.rez;
    }
    else if (obj.num1 !== "" && obj.num2 === "") {
        obj.rez = obj.num1;
        clear();
        display.value = obj.rez;
    }
    else if (obj.num1 === "") {
        display.value = obj.rez;
        clear();
    }

}

function clear() {
    obj.num1 = "";
    obj.num2 = "";
    obj.oper = "";
}

const add = (a, b) => parseFloat(a) + parseFloat(b);

const sub = (a, b) => parseFloat(a) - parseFloat(b);

const mult = (a, b) => parseFloat(a) * parseFloat(b);

const divi = (a, b) => {
    if (b == 0) {
        return "Не можна ділити на '0'";
    }
    return parseFloat(a) / parseFloat(b);
}

function calculate(a, b, op) {

    switch (op) {
        case "+":
            return add(a, b);
        case "-":
            return sub(a, b);
        case "*":
            return mult(a, b);
        case "/":
            return divi(a, b);
    }
}

function inputMemory(key) {
    if (key === "m+") {
        if (!obj.memr) {
            obj.memr = display.value;
        }
        else {
            obj.memr = parseFloat(obj.memr) + parseFloat(display.value);
        }
    }
    else if (key === "m-") {
        if (!obj.memr) {
            obj.memr = display.value;
        }
        else {
            obj.memr = parseFloat(obj.memr) - parseFloat(display.value);
        }
    }
    else if (key === "mrc") {
        if (obj.memr !== "") {
            obj.memr == display.value ? (obj.memr = "", display.value = "") : 
            (obj.num1 === "" ? (display.value = obj.memr, obj.num1 = obj.memr) :
            (display.value = obj.memr, obj.num2 = obj.memr));
        }
        else {
            display.value = "";
            clear();
        }
    }
}



