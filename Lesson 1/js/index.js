//   ----- ------  Завдання 1 ----- ---- --

document.write('<header style = " width: 800px; height: 200px; border: 3px solid red; text-align: center; box-sizing: border-box; color: grey;">header 800*200</header>');
document.write('<nav style = " width: 100px; height: 400px; border: 3px solid orange; text-align: center; display: inline-block; box-sizing: border-box; color: grey;">nav 100*400</nav>');
document.write('<section style = " width: 700px; height: 400px; border: 3px solid orange; text-align: center; display: inline-block; box-sizing: border-box; color: grey;">section 700*400</section>');
document.write('<footer style = " width: 800px; height: 200px; border: 3px solid grey; text-align: center; box-sizing: border-box; color: grey;">footer 800*200</footer>');


//   ----- ------  Завдання 2 ----- ---- --

var userName = prompt("Напишіть своє ім'я");
var userSurname = prompt("Напишіть своє прізвище");
var userAge = prompt("Скільки вам років?");

document.write(userName + "<br>");
document.write(userSurname + "<br>");
document.write(userAge + "<br>");


//   ----- ------  Завдання 3 ----- ---- --

var num1 = prompt("Введіть перше число", "1");
var num2 = prompt("Введіть друге число", "3");
var num3 = prompt("Введіть третє число", "5");

var number1 = parseFloat(num1);
var number2 = parseFloat(num2);
var number3 = parseFloat(num3);

var average = (number1 + number2 + number3) / 3;

alert("Середнє арифметичне чисел дорівнює " + average);


//   ----- ------  Завдання 4 ----- ---- --

var x = 6, y = 14, z = 4;
alert ( x += y - x++ * z );
/* Спочатку знаходимо інкремент числа x ( x++ це буде 6 ), потім множим його на число z ( 6*4=24 ),
далі - віднімаємо y - 24 ( 14 - 24 = -10 ), в кінці відбувається додавання та присвоєння значення 
змінній x ( x = 6 + (- 10) = -4 ) */

var x = 6, y = 14, z = 4;
alert ( z = --x - y *5 );
/* Спочатку знаходимо значення декременту --x ( буде 5 ), після цього відбувається множення 
y * 5 ( 14 * 5 = 70), потім віднімання ( 5 - 70 ), в кінці - присвоєння значення змінній z ( z = -65) */

var x = 6, y = 14, z = 4;
alert( y/= x + 5 % z );
/* Спочатку знаходимо залишок від ділення 5 % z ( 5 % 4 = 1 ), потім додавання x + 1 ( 6 + 1 = 7),
а в кінці - ділення та присвоєння значення змінній ( 14 / 7 = 2 ) */

var x = 6, y = 14, z = 4;
alert( z - x++ + y *5 );
/* Знаходимо інкремент числа x ( x++ це буде 6), потім відбувається множення y * 5 ( 14 * 5 = 70 ),
потім віднімання і додавання ( 4 - 6 + 70 = 68 ) */

var x = 6, y = 14, z = 4;
alert( x = y - x++ * z );
/* Знаходимо інкремент числа x ( x++ це буде 6), потім відбувається множення x++ *z ( 6 * 4 = 24 ),
і віднімання 14 - 24 = -10, в кінці присвоєння значення змінній x = -10  */


