import React from 'react';
import ReactDOM from 'react-dom';
import "./style.css";

const App = function () {
  return (
    <div className='app'>
      <div className='app-section'>
        <div className='app-title'>
          <Title></Title>
        </div>
        <div className='app-list'>
          <DaysOfWeek></DaysOfWeek>
        </div>
      </div>
      <div className='app-section'>
        <div className='app-title'>
          <TitleTwo></TitleTwo>
        </div>
        <div className='app-list'>
          <MonthsOfYear></MonthsOfYear>
        </div>
      </div>
      <div className='app-section'>
        <div className='app-title'>
          <TitleTree></TitleTree>
        </div>
        <div className='app-list'>
          <ZodiacSigns></ZodiacSigns>
        </div>
      </div>
    </div>
  )
}

const Title = function () {
  return (
    <h2>Дні тижня</h2>
  )
}

const DaysOfWeek = function () {
  return (
    <ul>
      <li>Понеділок</li>
      <li>Вівторок</li>
      <li>Середа</li>
      <li>Четвер</li>
      <li>П'ятниця</li>
      <li>Субота</li>
      <li>Неділя</li>
    </ul>
  )
}

const TitleTwo = function () {
  return (
    <h2>Місяці</h2>
  )
}

const MonthsOfYear = function () {
  return (
    <ul>
      <li>Січень</li>
      <li>Лютий</li>
      <li>Березень</li>
      <li>Квітень</li>
      <li>Травень</li>
      <li>Черіень</li>
      <li>Липень</li>
      <li>Серпень</li>
      <li>Вересень</li>
      <li>Жовтень</li>
      <li>Листопад</li>
      <li>Грудень</li>
    </ul>
  )
}

const TitleTree = function () {
  return (
    <h2>Знаки зодіаку</h2>
  )
}

const ZodiacSigns = function () {
  return (
    <ul>
      <li>Овен</li>
      <li>Телець</li>
      <li>Близнята</li>
      <li>Рак</li>
      <li>Лев</li>
      <li>Діва</li>
      <li>Терези</li>
      <li>Скорпіон</li>
      <li>Стрілець</li>
      <li>Козеріг</li>
      <li>Водолій</li>
      <li>Риби</li>
    </ul>
  )
}

ReactDOM.render(<App></App>, document.querySelector("#root"));