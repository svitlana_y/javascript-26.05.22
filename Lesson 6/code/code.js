// ------- Класна робота --------

function Calculator() {

    this.read = function() {
        this.number1 = parseFloat(prompt("Введіть перше значення"));
        this.number2 = parseFloat(prompt("Введіть друге значення"));
    };

    this.sum = function() {
        return this.number1 + this.number2;
    };

    this.mul = function() {
        return this.number1 * this.number2;
    };
}

let calculator = new Calculator();
calculator.read();

alert( "Sum=" + calculator.sum() );
alert( "Mul=" + calculator.mul() );


// ------- Домашня робота --------

// Завдання 1 -----

function Human(name, surname, age) {
    this.name = name;
    this.surname = surname;
    this.age = age;
}

let human1 = new Human("Ivan","Shevchenko", 27);
let human2 = new Human("Kate", "Voloshchuk", 19);
let human3 = new Human("Iryna", "Ganchik", 32);
let human4 = new Human("Inna", "Gaponchyk", 21);
let human5 = new Human("Volodymur", "Kozachenko", 38);

let humanArr = [human1, human2, human3, human4, human5];

console.dir(humanArr);

//збільшення
humanArr.sort((a, b) => a.age - b.age);
console.log("збільшення:");
humanArr.forEach((element) => console.log(element.age));

//зменшення
humanArr.sort((a, b) => b.age - a.age);
console.log("зменшення:");
humanArr.forEach((element) => console.log(element.age));



// Завдання 2 -----

function HumanFunc(name, surname, age, hobby) {
    this.name = name;
    this.surname = surname;
    this.age = age;
    this.hobby = hobby;
    this.hello = function() {
        return `Доброго дня, мене звати ${this.name} ${this.surname} і мені ${this.age} років. Моє хоббі - це ${this.hobby}.`;
    };
}

let friendA = new HumanFunc("Olya", "Kachunska", 25, "music");

alert(friendA.hello());

//на основі прототиту додаємо властивості і методи екземпляру
let friendB = new HumanFunc("Mykola", "Boyko", 32, "drawing");
friendB.study = "Kyiv Polytechnic Institute";
friendB.endStudy = 2015;

friendB.hi = function() {
    return `${this.hello()} Я закінчив ${this.study} у ${this.endStudy} році.`;
};

alert(friendB.hi());


