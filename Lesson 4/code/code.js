// -----  Завдання 1

let numberArray = [6, 28, 73, "hello", 1, 7];

function multiplyByThree(element) {
    let elementType = typeof(element);
    if (elementType != "number") {
        return 0;
    }
    return element * 3;
}

function map(fn, array) {
    // 0. creates new array
    let newArray = [];
    // 1. handles each element of array by fn
    // 2. pushes result of fn to a new array
    for (let i = 0; i < array.length; i++) {
        const element = array[i];
        let functionResult = fn(element);
        newArray.push(functionResult);
        
    }
    // 3. returns new array
    return newArray;
}

let result = map(multiplyByThree, numberArray);

document.write("Old array: " + numberArray + "<br>");
document.write("New array: " + result + "<br>");



// -----  Завдання 2

function checkAge(age) {
    let result = age > 18 ? true : confirm("Батьки дозволили?");
    return result;
}

let checkAgeResult = checkAge(9);
document.write("Age result: " + checkAgeResult +"<br>");

document.write("New array: " + result);
