// -----  Завдання 1
console.log('-----  Завдання 1 -----');

let inputNumber = prompt("Введіть число від 1 до 7");

function dayOfWeek(number) {
    switch(number) {
        case '1':
          return "Понеділок";
        case '2':
          return "Вівторок";
        case '3':
          return "Середа";
        case '4':
          return "Четвер";
        case '5':
          return "П'ятниця";
        case '6':
          return "Субота";
        case '7':
          return "Неділя";
        default:
          return "Помилка! Число повинно бути від 1 до 7";
      }
}

let result = dayOfWeek(inputNumber);

console.log(result);


// -----  Завдання 2
console.log('-----  Завдання 2 -first way ----');

let text = "var_text_hello";

//  first way - prototype implementation:

String.prototype.toPascalCase = function () {
  if (this.length === 0) { return this }
  let firstLetter = this[0];
  let firstLetterUpperCase = this[0].toUpperCase();
  return this.replace(firstLetter, firstLetterUpperCase);
}

splittedText = text.split("_");

for (let i = 0; i < splittedText.length; i++) {
  splittedText[i] = splittedText[i].toPascalCase()
}

text = splittedText.join("");
console.log(text);


console.log('-----  Завдання 2 -second way ----');
// second way - simple implementation:

text = "var_text_hello";

for (let i = 0; i < splittedText.length; i++) {
  let firstLetter = splittedText[i][0];
  let firstLetterUpperCase = splittedText[i][0].toUpperCase();
  splittedText[i] = splittedText[i].replace(firstLetter, firstLetterUpperCase);
}

text = splittedText.join("");
console.log(text);



// -----  Завдання 3
console.log('-----  Завдання 3 -----');

function arrayFilling(array) {
  for (let i = 0; i < 10; i++) {
    array.push("X");
  }
}

let testArr = [];
arrayFilling(testArr);

console.log(testArr);



// -----  Завдання 4
console.log('-----  Завдання 4 -----');

let array50 = new Array(50);

function printElements(arr) {
  for (let a = 0; a < arr.length; a++) {
    arr[a] = a + 1;
  }
}

printElements(array50);
console.log(array50);

let oddNumbers = array50.filter(elem => elem % 2 == true);
document.getElementById("oddNumbers").innerHTML = (`Непарні числа з мaсива на 50 елементів: ${oddNumbers}`);

let evenNumbers = array50.filter(elem => elem % 2 === 0);
document.getElementById("evenNumbers").innerHTML = (`Парні числа з мaсива на 50 елементів: ${evenNumbers}`);



// -----  Завдання 5
console.log('-----  Завдання 5 -----');

let newNumber = parseFloat(prompt("Введіть будь яке число"));

function one() {
  alert("один!");
}

function two() {
  alert("два!");
}

let ggg = newNumber > 0 ? one : two;

ggg();


// -----  Завдання 6
console.log('-----  Завдання 6 -----');

const add = (a, b) => {
  return a + b;
}

const sub = (a, b) => {
  return a - b;
}

const mult = (a, b) => {
  return a * b;
}

const divi = (a, b) => {
  if (b === 0) {
    console.error("Помилка! На нуль ділити не можемо");
  }
  return a / b;
}

function show (data) {
  document.getElementById("culculate").innerHTML = (`Результат дії калькулятора: ${data}`);
}

function calculate (num1, num2, callback, show) {
  show(callback(num1, num2));
}

const oper1 = parseFloat(prompt("Число 1")),
oper2 = parseFloat(prompt("Число 2")),
sign = prompt("Введіть знак арифметичної операції");

if (isNaN(oper1) || isNaN(oper2) ) {
  console.error("Помилка! Необхідно ввести число");
}

switch (sign) {
  case "+":
    calculate(oper1, oper2, add, show);
    break;
    case "-":
    calculate(oper1, oper2, sub, show);
    break;
    case "*":
    calculate(oper1, oper2, mult, show);
    break;
    case "/":
    calculate(oper1, oper2, divi, show);
    break;
}

// -----  Завдання 7
console.log('-----  Завдання 7 -----');

function ggg1(func1, func2) {
  return func1() + func2();
}

let resuLt = ggg1(() => 3, () => 4);

console.log(resuLt);

// -----  Завдання 8
console.log('-----  Завдання 8 -----');

function f1() {
  f1.count++;
  console.log(`Функція викликалась ${f1.count} разів`)
}

f1.count = 0;

for (let i = 0; i < 5; i++) {
  f1();
}

console.log(f1.count);