
// Завдання 1
// Створи клас, який буде створювати користувачів з ім'ям та прізвищем. 
// Додати до класу метод для виведення 
// імені та прізвища

class Student {
    constructor(firstName, surname) {
        this.firstName = firstName;
        this.surname = surname;
    }
    printName() {
        alert("Привіт. Мене звати " + this.firstName + this.surname);
    }
}

let studentOne = new Student("Ivan ", "Tokar");
studentOne.printName();


// Завдання 2
// Створи список, що складається з 4 аркушів. Використовуючи джс зверніся 
// до 2 li і з використанням навігації 
// по DOM дай 1 елементу синій фон, а 3 червоний


let  ull = document.createElement("ul");
let li;

document.body.append(ull);

for (i = 0; i < 4; i++) {
    li = document.createElement("li");
    ull.append(li);
    li.innerHTML = "Список " + i;
}

let list = document.getElementsByTagName("li");
let second = list[1];

second.previousElementSibling.style.backgroundColor = "blue";
second.nextElementSibling.style.backgroundColor = "red";


// Завдання 3
// Створи див висотою 400 пікселів і додай на нього подію наведення мишки. 
// При наведенні мишки виведіть текст 
// координати, де знаходиться курсор мишки
 
let div = document.createElement("div");
li.after(div);

div.classList.add("yellow");

div.addEventListener("mousemove", (e) => {
    div.innerHTML = "Координати по х: " + e.clientX 
    + " та по у: " + e.clientY;
});



// Завдання 4
// Створи кнопки 1,2,3,4 і при натисканні на кнопку виведи інформацію про те, 
// яка кнопка була натиснута

let div2 = document.createElement("div");
document.body.append(div2);
let button; 

for (i = 1; i < 5; i++) {
    button = document.createElement("button");
    div2.append(button);
    button.innerHTML = "Кнопка " + i;
}

div2.addEventListener("click", (e) => {
    let span = document.createElement("span");
    e.target.after(span);
    span.innerHTML = "Була натиснута " + e.target.innerHTML;

    setTimeout(() => {
        span.remove();
    }, 2000);
});



// Завдання 5
// Створи див і зроби так, щоб при наведенні на див див змінював своє положення 
// на сторінці

let box = document.createElement("div");
document.body.append(box);
box.classList.add("green");

box.addEventListener("mouseenter", (e) => {
    box.style.transform = "translate(50px, 25px)";
});

box.addEventListener("mouseleave", (e) => {
    box.style.transform = "translate(0px, 0px)";
});



// Завдання 6
// Створи поле для введення кольору, коли користувач вибере якийсь колір, зроби 
// його фоном body

let fild = document.createElement("input");

document.body.append(fild);
fild.setAttribute("type", "color");

fild.addEventListener("input", (e) => {
    let body = document.getElementById("body");
    body.style.backgroundColor = fild.value;
})


// Завдання 7
// Створи інпут для введення логіну, коли користувач почне вводити дані в інпут 
// виводь їх в консоль

let login = document.createElement("input");

document.body.append(login);

login.addEventListener("input", () => {
    console.log(login.value);
});