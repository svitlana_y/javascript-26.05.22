//   --   Завдання 1   - -   

const div1 = document.getElementById("div1");

console.log(getStyle(div1, "margin"));

function getStyle(element, styleName) {
    return window.getComputedStyle(element)[styleName];
}


//   --   Завдання 2   - -  

let secondsCounter = 0;
let minutsCounter = 0;
let hoursCounter = 0;
let intervalHandlerTime;
let timer = false;

const seconds = document.getElementById("seconds");
const minuts = document.getElementById("minuts");
const hours = document.getElementById("hours");

const background = document.querySelector(".container-stopwatch");

const get = id => document.getElementById(id);

const showTime = () => {
    seconds.innerText = secondsCounter.toString().padStart(2, '0');
    minuts.innerText = minutsCounter.toString().padStart(2, '0');
    hours.innerText = hoursCounter.toString().padStart(2, '0');
}

const removeClass = (element, param) => {
    if (element.classList.contains(param)) {
        element.classList.remove(param);
    }
}

const countTime = () => {

    secondsCounter++;
    if (secondsCounter > 59) {
        secondsCounter = 0;
        minutsCounter += 1;
    }

    if (minutsCounter > 59) {
        minutsCounter = 0;
        hoursCounter += 1;
    }

    if (hoursCounter > 24) {
        hoursCounter = 0;
    }
    showTime();
}

get("start").onclick = () => {

    intervalHandlerTime = setInterval(countTime, 1000);
    removeClass(background, "silver");
    removeClass(background, "red");
    removeClass(background, "black");
    background.classList.add("green");
}

get("stop").onclick = () => {
    clearInterval(intervalHandlerTime);
    removeClass(background, "silver");
    removeClass(background, "green");
    removeClass(background, "black");
    background.classList.add("red");
}

get("reset").onclick = () => {
    secondsCounter = 0;
    minutsCounter = 0;
    hoursCounter = 0;
    showTime();
    clearInterval(intervalHandlerTime);
    removeClass(background, "green");
    removeClass(background, "red");
    removeClass(background, "black");
    background.classList.add("silver");
}


//   --   Завдання 3   - - 

function ValidPhone() {
    const regex = /^\d{3}-\d{3}-\d{2}-\d{2}$/;
    const myPhone = document.getElementById("phone").value;
    let valid = regex.test(myPhone);
    if (valid) {
        document.getElementById("phone").style.backgroundColor = "green";
        document.location.href = "https://risovach.ru/upload/2013/03/mem/toni-stark_13447470_big_.jpeg";
    } else {
        document.getElementById("error").innerText = "Помилка! Введенний формат телефону не вірний!";
    }
}


//   --   Завдання 4   - - 

let imageSlider = document.getElementById("imageSlider");

let imageIndex = 0;

let images = [
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR92G98Pq8bkV0a-0ujN3eqGYfB2okDzqoYYg&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRWk9yjyfJcLE02T4wVix9TibdeYa8DnTcNxw&usqp=CAU", 
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcT6i2qEh3LdL0ZCkVKrtc9Itbktd2PjvrkPlg&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRGnbwTWm8NGLN4VY4HGBNvekmXG7ViucE0sg&usqp=CAU",
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRYD-J5g0pfMKLRD4d6eYjkN5kY-rinWbSL2Q&usqp=CAU"
];

let autoSlider = () => {
    imageIndex++;
    imageSlider.setAttribute("src", images[imageIndex]);
    if(imageIndex === images.length) {
        imageIndex = 0;
    }
}

let showImages = () => {
    setInterval(autoSlider, 3000);
}

showImages();
