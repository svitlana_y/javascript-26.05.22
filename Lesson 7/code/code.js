//  - - - Завдання 1 - - - Гра


const words = [
    "програма",
    "чудовий",
    "оладка",
    "апельсин"
];

// Обираємо рандомне слово
const word = words[Math.floor(Math.random() * words.length)];

// Створюємо кінцевий масив
const answerArray = [];

for (let i = 0; i < word.length; i++) {
    answerArray[i] = "_";
}

let remainingLetters = word.length;

// Ігровий цикл
while (remainingLetters > 0) {

    // Показуємо стан гри
    alert(answerArray.join(" "));

    // Запитуємо варіант відповіді
    const guess = prompt("Вгадайте букву або натисніть Відміна для виходу із гри");

    if (guess === null) {
        // Виходимо з шгрового циклу
        break;
    } else if (guess.length !== 1) {
        alert("Введіть одну букву");
    } else {


        for (let j = 0; j < word.length; j++) {

            // Перевіряємо, щоб введена буква не повторювалась, якщо вона вже відгадана
            if (guess === answerArray[j] && answerArray[j] !== "_") {
                alert("Буква вже вгадана! Введіть іншу букву");
                break;
            }
            // Оновлюємо стан гри
            else if (word[j] === guess) {
                answerArray[j] = guess;
                remainingLetters--;
            }
        }
    }

}
// Кінець ігрового циклу

// Показуємо відповідь та вітаємо ігрока
if (remainingLetters === 0) {
    alert(answerArray.join(" "));
    alert(`Чудово! Було загадано слово: ${word} `);
}



//  - - - Завдання 2 + Завдання 3 - - -

class User {
    constructor(firstName, lastName, birthday) {

        this.firstName = firstName;
        this.lastName = lastName;
        this.birthday = birthday;
    }

    static buildUser() {
        const firstName = prompt("Enter your first-name");
        const lastName = prompt("Enter your last-name");
        const birthday = prompt("Enter your date of birth", "dd.mm.yyyy");

        return new User(firstName, lastName, birthday);
    }

    getLogin() {
        return (this.firstName[0] + this.lastName).toLowerCase();
    }

    getAge() {
        const currentYear = 2022;
        const age = currentYear - this.birthday.split(".")[2];
        
        return age;
    }

    getPassword() {
        const password = this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.split(".")[2];
        return password;
    }

}

let user = User.buildUser();

console.log("Логін: " + user.getLogin());

console.log("Вік: " + user.getAge());

console.log("Пароль: " + user.getPassword());


//  - - - Завдання 4 - - -


function filterBy(arr, type) {
    let newArr = [];
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] !== type) {
            newArr.push(arr[i]);
        }
    }
    return newArr;
}

let array = ["hello", "world", 23, "23", null];
let typeOfElem = "string";

console.log(filterBy(array, typeOfElem));

