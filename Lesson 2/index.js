//  Завдання 1. Переписати if з використанням оператора '?':

var a, b;

var result = (a + b < 4 ) ? "Мало" : "Багато";

// Завдання 2. Переписати if ... else  з використанням декількох операторів "?":

var message;
var login;

message = login == "Вася" ? "Привіт" :
    login == "Директор" ? "Доброго дня" :
        login == "" ? "Немає логіну" : "";


//  Завдання 3

let a_ = +prompt("Введіть число A");
let b_ = +prompt("Введіть число В, яке буде більше ніж A");
let sum = 0;

while (a_ > b_) {
    console.error("Помилка, число А має бути більше ніж В");
    a_ = +prompt("Введіть число A");
    b_ = +prompt("Введіть число В, яке буде більше ніж A");
}

for ( ; a_ <= b_; a_++) {
    sum += a_;
    if ( a_ % 2 == 1) {
        console.log(a_);  // виводить непарні числа
    }
}
console.log(sum);         // виводить суму чисел у проміжку а і b



// 4.  Малюємо Прямокутник  

for (let i = 0; i < 4; i++) {
    for (let j = 0; j < 20; j++) {
        document.write("* ");
    }
    document.write("<br></br>");
}

document.write("<hr></hr>");


//  Прямокутний трикутник  

for (let i = 0; i < 10; i++) {
    for (let j = 0; j < i; j++) {
        document.write("* ");
    }
    document.write("<br></br>");
}

document.write("<hr></hr>");

//  Рівносторонній трикутник 

for (let i = 0; i < 10; i++) {
    for (let k = (10 - i); k > 0; k--) {
        document.write("&#8194;");
    }
    for (let j = -1; j < i; j++) {
        document.write("*");
    }
    for (let j = 0; j < i; j++) {
        document.write("*");
    }
    document.write("<br></br>");
}

document.write("<hr></hr>");

// Ромб

let stars = 1;
let height = 12;
const delta = 2;
const star = "&";
const space = "&#8194;";

for (let i = 0; i < height; i++) {
    for (let k = (height - i); k > 0; k--) {
        document.write(space);
    }
    for (let j = 0; j < stars; j++) {
        document.write(star);
    }
    stars = i === height - 1 ? stars : stars + delta;
    document.write("<br></br>");
}
for (let i = 0; i < height - 1; i++) {
    for (let k = -delta; k < i; k++) {
        document.write(space);
    }
    stars -= delta;
    for (let j = 0; j < stars; j++) {
        document.write(star);
    }
    document.write("<br></br>");
}

