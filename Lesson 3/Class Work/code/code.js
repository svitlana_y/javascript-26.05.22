"use strict";
//   ----- 1 -----
let a = ["a", "b", "c"], b = [1, 2, 3];
let add = a.concat(b);
console.log(add);
console.log(a);

//   ----- 2 -----
a.push(1, 2, 3);
console.log(a);

//   ----- 3 -----
b.reverse();
console.log(b);


//   ----- 4 -----
b.reverse();
b.push(4, 5, 6);
console.log(b);

//   ----- 5 -----
b = [1, 2, 3];
b.unshift(4, 5, 6);
console.log(b);

//   ----- 6 -----
let c =["js", "css", "jd"];
console.log(c[0]);

//   ----- 7 -----
let d = [1, 2, 3, 4, 5];
let slised = d.slice(0, 3);
console.log(slised);

//   ----- 8 -----
d.splice(1, 2);
console.log(d);

//   ----- 9 -----
d = [1, 2, 3, 4, 5];
d.splice(2, 3, 10, 3, 4, 5);
console.log(d);

//   ----- 10 -----
let e = [3, 4, 1, 2, 7];
e.sort();
console.log(e);

//   ----- 11 -----
let f = ["Привіт, ", "світ", "!"];
console.log(f.join(""));

//   ----- 12 -----
f[0] = "Бувай, ";
console.log(f.join(""));

//   ----- 13 -----
//let arr = [1, 2, 3, 4, 5];            --- перший спосіб
let arr = new Array (1, 2, 3, 4, 5);  // ---другий спосіб
console.log(arr);

//   ----- 15 -----
arr = ["a", "b", "c", "d"];
console.log(`${arr[0]}+${arr[1]}, ${arr[2]}+${arr[3]}`);

//   ----- 16 -----
let amoundElements = parseInt(prompt("Введіть кількість єлементів масиву"));
let arrElem = new Array(amoundElements);

for (let i = 0; i < arrElem.length; i++) {
    arrElem[i] = i;
}
console.log(arrElem);

//   ----- 17 -----
let odd = [];
let even = [];
for (let i = 0; i < arrElem.length; i++) {
    if (i % 2 === 1) {
        odd.push(i);
    }
    else {
        even.push(i);
    }
}

document.getElementById("odd").innerHTML = odd;

let evenObject = document.getElementById("even");
evenObject.style = 'background-color:red';
evenObject.innerHTML = `${even} </span>`;

document.write("<br>");

//   ----- 18 -----

var vegetables = ["Капуста", "Репа", "Редиска", "Морковка"];

function arrayToString(arr) {
    return arr.join(", ");
}

let str1 = arrayToString(vegetables);

document.write(str1);
