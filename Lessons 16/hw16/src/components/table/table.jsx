import React from "react";
import "./table.css";

function Table(props) {
  console.log(props);
  return (
    <table>
      <tbody>
        <tr>
          <td>Номер валюти</td>
          <td>Назва валюти</td>
          <td>Ціна валюти</td>
        </tr>
        {props.data.map((element) => {
          return (
            <tr>
              <td>{element.r030}</td>
              <td>{element.txt}</td>
              <td>{element.rate}</td>
            </tr>
            );
        })}
      </tbody>
    </table>
  );
}



export default Table