window.onload = () => {
    const btn = document.querySelector("#btn");

    //replace button to input
    const newInput = () => {
        let input = document.createElement("input");

        btn.replaceWith(input);
        input.classList = "input";

        //create button "draw" after input
        let drawCircles = document.createElement("button");
        drawCircles.innerText = "Намалювати";
        drawCircles.setAttribute("class", "circles");
        input.after(drawCircles);

        drawCircles.onclick = () => {
            let circleDiametr = document.querySelector(".input").value;

            //create circles
            for (i = 0; i < 100; i++) {
                let div = document.createElement("div");

                document.body.append(div);

                div.style.width = circleDiametr + "px";
                div.style.height = circleDiametr + "px";
                div.style.backgroundColor = `hsl(${Math.floor(Math.random() * 360)}, 50%, 50%)`;
                div.style.display = "inline-block";
                div.style.borderRadius = "50%";

                // remove circle
                div.onclick = () => {
                    div.remove();
                };
            };
        };
    };
    btn.addEventListener("click", newInput);
};

