document.addEventListener("keydown", (e) => {

        let [...buttons] = document.getElementsByClassName("btn");

        buttons.forEach((elem) => {

            if (e.code.replace("Key", "") === elem.innerHTML) {
                elem.classList.add("blue");
            } else {
                elem.classList.remove("blue");
            }
        });
    });
